package com.atguigu.gmallpublisher.controller;

import com.atguigu.gmallpublisher.service.SugarService;
import com.atguigu.gmallpublisher.utils.DateFormatUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

//@Controller
@RestController //=@Controller + 所有的方法@ResponseBody
@RequestMapping("/sugar")
public class SugarController {

    @Autowired
    private SugarService sugarService;

    @RequestMapping("/test")
    //@ResponseBody
    public String test1() {
        System.out.println("aaaaaaaaaa");
        return "success";
    }

    @RequestMapping("/test2")
    public String test2(@RequestParam("nn") String name,
                        @RequestParam(value = "age", defaultValue = "18") int age) {
        System.out.println(name + ":" + age);
        return "success";
    }

    @RequestMapping("/gmv")
    public String getGmv(@RequestParam(value = "date", defaultValue = "0") int date) {

        if (date == 0) {
            date = getToday();
        }

        //查询GMV
        BigDecimal gmv = sugarService.getGMV(date);

        //拼接JSON并返回
        return "{\n" +
                "  \"status\": 0,\n" +
                "  \"msg\": \"\",\n" +
                "  \"data\": " + gmv + "\n" +
                "}";
    }

    @RequestMapping("trademark")
    public String getGmvByTm(@RequestParam(value = "date", defaultValue = "0") int date,
                             @RequestParam(value = "limit", defaultValue = "5") int limit) {

        if (date == 0) {
            date = getToday();
        }

        Map<String, BigDecimal> gmvByTm = sugarService.getGmvByTm(date, limit);
        Set<String> columns = gmvByTm.keySet();
        Collection<BigDecimal> values = gmvByTm.values();

        return "{\n" +
                "  \"status\": 0,\n" +
                "  \"msg\": \"\",\n" +
                "  \"data\": {\n" +
                "    \"categories\": [\"" +
                StringUtils.join(columns, "\",\"") +
                "\"],\n" +
                "    \"series\": [\n" +
                "      {\n" +
                "        \"name\": \"品牌\",\n" +
                "        \"data\": [" +
                StringUtils.join(values, ",") +
                "]" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

    }

    private int getToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return Integer.parseInt(sdf.format(System.currentTimeMillis()));
    }

}
