package com.atguigu.gmallpublisher.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface SugarMapper {

    @Select("select sum(order_origin_total_amount) total_amount from dws_trade_sku_order_window where toYYYYMMDD(stt)=#{date}")
    BigDecimal selectGmv(int date);

    @Select("select trademark_id,trademark_name,sum(order_origin_total_amount) total_amount from dws_trade_sku_order_window where toYYYYMMDD(stt)=#{date} group by trademark_id,trademark_name order by total_amount desc limit #{limit}")
    List<Map> selectGmvByTm(@Param("date") int date,@Param("limit") int limit);

}
