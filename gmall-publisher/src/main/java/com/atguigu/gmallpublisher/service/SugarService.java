package com.atguigu.gmallpublisher.service;

import java.math.BigDecimal;
import java.util.Map;

public interface SugarService {

    BigDecimal getGMV(int date);

    Map<String,BigDecimal> getGmvByTm(int date, int limit);

}
