package com.atguigu.gmallpublisher.service.impl;

import com.atguigu.gmallpublisher.mapper.SugarMapper;
import com.atguigu.gmallpublisher.service.SugarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class SugarServiceImpl implements SugarService {

    @Autowired
    private SugarMapper sugarMapper;

    @Override
    public BigDecimal getGMV(int date) {
        return sugarMapper.selectGmv(date);
    }

    @Override
    public Map<String, BigDecimal> getGmvByTm(int date, int limit) {

        HashMap<String, BigDecimal> resultMap = new HashMap<>();

        List<Map> mapList = sugarMapper.selectGmvByTm(date, limit);

        for (Map map : mapList) {
            resultMap.put((String) map.get("trademark_name"), (BigDecimal) map.get("total_amount"));
        }

        return resultMap;
    }
}
