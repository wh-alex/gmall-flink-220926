package com.atguigu.app.dwd.db;

import com.atguigu.utils.MyKafkaUtil;
import com.atguigu.utils.MySqlUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

//数据流:web/app -> 业务服务器(Mysql) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD)
//程 序:Mock -> Mysql -> Maxwell -> Kafka(ZK) -> Dwd02_TradeCartAdd -> Kafka(ZK)
public class Dwd02_TradeCartAdd {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.使用FlinkSQL方式读取Kafka topic_db 主题数据创建表
        tableEnv.executeSql(MyKafkaUtil.getTopicDbDDL("cart_add_220926"));

        //打印测试
        //tableEnv.sqlQuery("select * from topic_db").execute().print();

        //TODO 3.过滤出加购数据
        Table cartInfoTable = tableEnv.sqlQuery("" +
                "select\n" +
                "    `data`['id'] id,\n" +
                "    `data`['user_id'] user_id,\n" +
                "    `data`['sku_id'] sku_id,\n" +
                "    `data`['cart_price'] cart_price,\n" +
                "    `data`['sku_num'] sku_num,\n" +
                "    `data`['sku_name'] sku_name,\n" +
                "    `data`['is_checked'] is_checked,\n" +
                "    `data`['create_time'] create_time,\n" +
                "    `data`['operate_time'] operate_time,\n" +
                "    `data`['is_ordered'] is_ordered,\n" +
                "    `data`['order_time'] order_time,\n" +
                "    `data`['source_type'] source_type,\n" +
                "    `data`['source_id'] source_id,\n" +
                "    pt\n" +
                "from topic_db\n" +
                "where `database` = 'gmall-220926-flink'\n" +
                "and `table` = 'cart_info'\n" +
                "and `type` = 'insert'");

        tableEnv.createTemporaryView("cart_info", cartInfoTable);

        //打印测试
        //tableEnv.sqlQuery("select * from cart_info").execute().print();

        //TODO 4.构建 base_dic 维表
        tableEnv.executeSql(MySqlUtil.getBaseDicLookUpTable());

        //TODO 5.两表关联
        Table resultTable = tableEnv.sqlQuery("" +
                "select\n" +
                "    cart.id,\n" +
                "    cart.user_id,\n" +
                "    cart.sku_id,\n" +
                "    cart.cart_price,\n" +
                "    cart.sku_num,\n" +
                "    cart.sku_name,\n" +
                "    cart.is_checked,\n" +
                "    cart.create_time,\n" +
                "    cart.operate_time,\n" +
                "    cart.is_ordered,\n" +
                "    cart.order_time,\n" +
                "    cart.source_type,\n" +
                "    dic.dic_name,\n" +
                "    cart.source_id\n" +
                "from cart_info cart\n" +
                "join base_dic FOR SYSTEM_TIME AS OF cart.pt AS dic\n" +
                "on cart.source_type=dic.dic_code");

        tableEnv.createTemporaryView("result_table", resultTable);

        //TODO 6.创建 DWD层 Kafka加购事实表
        tableEnv.executeSql("" +
                "create table dwd_cart_info(\n" +
                "    `id` STRING,\n" +
                "    `user_id` STRING,\n" +
                "    `sku_id` STRING,\n" +
                "    `cart_price` STRING,\n" +
                "    `sku_num` STRING,\n" +
                "    `sku_name` STRING,\n" +
                "    `is_checked` STRING,\n" +
                "    `create_time` STRING,\n" +
                "    `operate_time` STRING,\n" +
                "    `is_ordered` STRING,\n" +
                "    `order_time` STRING,\n" +
                "    `source_type` STRING,\n" +
                "    `dic_name` STRING,\n" +
                "    `source_id` STRING\n" +
                ")" + MyKafkaUtil.getKafkaSinkDDL("dwd_trade_cart_add"));

        //TODO 7.将数据写出
        tableEnv.executeSql("insert into dwd_cart_info select * from result_table");

    }
}