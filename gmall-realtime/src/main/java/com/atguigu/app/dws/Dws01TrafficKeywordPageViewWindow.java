package com.atguigu.app.dws;

import com.atguigu.bean.KeywordBean;
import com.atguigu.func.SplitFunction;
import com.atguigu.utils.ClickHouseUtil;
import com.atguigu.utils.MyKafkaUtil;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

//数据流:web/app -> 日志服务器(file) -> flume -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> ClickHouse(DWS)
//程 序:Mock -> file -> f1.sh -> Kafka(ZK) -> BaseLogApp -> Kafka(ZK) -> Dws01TrafficKeywordPageViewWindow -> ClickHouse(ZK)
public class Dws01TrafficKeywordPageViewWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.使用FlinkSQL读取Kafka页面日志主题数据,同时提取时间戳生成WaterMark
        String topic = "dwd_traffic_page_log";
        String groupId = "keyword_220926";
        tableEnv.executeSql("" +
                "create table page_log(\n" +
                "    `common` MAP<STRING,STRING>,\n" +
                "    `page` MAP<STRING,STRING>,\n" +
                "    `ts` BIGINT,\n" +
                "    rt AS TO_TIMESTAMP_LTZ(ts,3),\n" +
                "    WATERMARK FOR rt AS rt - INTERVAL '2' SECOND\n" +
                ")" + MyKafkaUtil.getKafkaDDL(topic, groupId));

        //TODO 3.过滤出搜索数据
        Table filterTable = tableEnv.sqlQuery("" +
                "select\n" +
                "    `page`['item'] item,\n" +
                "    rt\n" +
                "from page_log\n" +
                "where `page`['last_page_id'] = 'search'\n" +
                "and `page`['item_type'] = 'keyword'\n" +
                "and `page`['item'] is not null");
        tableEnv.createTemporaryView("filter_table", filterTable);

        //TODO 4.注册自定义UDTF函数
        tableEnv.createTemporarySystemFunction("SplitFunction", SplitFunction.class);

        //TODO 5.使用自定义函数切词
        Table splitTable = tableEnv.sqlQuery("" +
                "SELECT \n" +
                "    rt, \n" +
                "    word\n" +
                "FROM filter_table, \n" +
                "LATERAL TABLE(SplitFunction(item))");
        tableEnv.createTemporaryView("split_table", splitTable);

        //TODO 6.分组开窗聚合
        Table resultTable = tableEnv.sqlQuery("" +
                "SELECT\n" +
                "    date_format(window_start,'yyyy-MM-dd HH:mm:ss') stt,\n" +
                "    date_format(window_end,'yyyy-MM-dd HH:mm:ss') edt,\n" +
                "    word keyword,\n" +
                "    count(*) keyword_count,\n" +
                "    UNIX_TIMESTAMP() ts\n" +
                "FROM TABLE( \n" +
                "    TUMBLE(TABLE split_table, DESCRIPTOR(rt), INTERVAL '10' SECONDS))\n" +
                "GROUP BY word,window_start, window_end");
        tableEnv.createTemporaryView("result_table", resultTable);

        //打印测试
        //tableEnv.sqlQuery("select * from result_table").execute().print();

        //TODO 7.将数据写出到ClickHouse
        DataStream<KeywordBean> keywordBeanDataStream = tableEnv.toAppendStream(resultTable, KeywordBean.class);
        keywordBeanDataStream.print(">>>>>>>>>>");
        keywordBeanDataStream.addSink(ClickHouseUtil.getSinkFunction("insert into dws_traffic_keyword_page_view_window values(?,?,?,?,?)"));

        //TODO 8.启动任务
        env.execute("Dws01TrafficKeywordPageViewWindow");

    }
}