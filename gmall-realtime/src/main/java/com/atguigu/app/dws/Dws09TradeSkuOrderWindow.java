package com.atguigu.app.dws;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TradeSkuOrderBean;
import com.atguigu.func.AsyncDimFunction;
import com.atguigu.utils.*;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

//数据流:web/app -> 业务服务器(Mysql) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> ClickHouse(DWS)
//程 序:Mock -> Mysql -> Maxwell -> Kafka(ZK) -> Dwd03_TradeOrderDetail -> Kafka(ZK) -> Dws09TradeSkuOrderWindow(Redis,Phoenix) -> ClickHouse(ZK)
public class Dws09TradeSkuOrderWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.读取Kafka DWD层 订单明细主题数据
        String topic = "dwd_trade_order_detail";
        String groupId = "dws_trade_sku_order_0926";
        DataStreamSource<String> kafkaDS = env.addSource(MyKafkaUtil.getFlinkKafkaConsumer(topic, groupId));

        //TODO 3.过滤null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    JSONObject jsonObject = JSONObject.parseObject(value);
                    out.collect(jsonObject);
                }
            }
        });

        //TODO 4.提取时间戳
        SingleOutputStreamOperator<JSONObject> jsonObjWithWMDS = jsonObjDS.assignTimestampsAndWatermarks(WatermarkStrategy.<JSONObject>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
            @Override
            public long extractTimestamp(JSONObject element, long recordTimestamp) {
                return element.getLong("create_time");
            }
        }));

        //TODO 5.按照订单明细ID分组
        KeyedStream<JSONObject, String> keyedByDetailIdDS = jsonObjWithWMDS.keyBy(json -> json.getString("id"));

        //TODO 6.去重并转换为JavaBean对象
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderDS = keyedByDetailIdDS.flatMap(new RichFlatMapFunction<JSONObject, TradeSkuOrderBean>() {

            private ValueState<String> existState;

            @Override
            public void open(Configuration parameters) throws Exception {
                StateTtlConfig ttlConfig = new StateTtlConfig.Builder(Time.seconds(5))
                        .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                        .build();
                ValueStateDescriptor<String> stateDescriptor = new ValueStateDescriptor<>("exist", String.class);
                stateDescriptor.enableTimeToLive(ttlConfig);

                existState = getRuntimeContext().getState(stateDescriptor);
            }

            @Override
            public void flatMap(JSONObject value, Collector<TradeSkuOrderBean> out) throws Exception {

                //取出状态数据
                String exists = existState.value();

                if (exists == null) {
                    existState.update("1");

                    BigDecimal splitActivityAmount = value.getBigDecimal("split_activity_amount");
                    if (splitActivityAmount == null) {
                        splitActivityAmount = new BigDecimal("0.0");
                    }

                    BigDecimal splitCouponAmount = value.getBigDecimal("split_coupon_amount");
                    if (splitCouponAmount == null) {
                        splitCouponAmount = new BigDecimal("0.0");
                    }

                    out.collect(TradeSkuOrderBean.builder()
                            .skuId(value.getString("sku_id"))
//                            .skuName("")
                            .orderAmount(value.getBigDecimal("split_total_amount"))
                            .originalAmount(value.getBigDecimal("split_original_amount"))
                            .activityAmount(splitActivityAmount)
                            .couponAmount(splitCouponAmount)
                            .build());
                }
            }
        });

        //TODO 7.按照SKU_ID分组、开窗、聚合
        SingleOutputStreamOperator<TradeSkuOrderBean> reduceDS = tradeSkuOrderDS.keyBy(TradeSkuOrderBean::getSkuId)
                .window(TumblingEventTimeWindows.of(org.apache.flink.streaming.api.windowing.time.Time.seconds(10)))
                .reduce(new ReduceFunction<TradeSkuOrderBean>() {
                    @Override
                    public TradeSkuOrderBean reduce(TradeSkuOrderBean value1, TradeSkuOrderBean value2) throws Exception {
                        value1.setOriginalAmount(value1.getOriginalAmount().add(value2.getOriginalAmount()));
                        value1.setOrderAmount(value1.getOrderAmount().add(value2.getOrderAmount()));
                        value1.setActivityAmount(value1.getActivityAmount().add(value2.getActivityAmount()));
                        value1.setCouponAmount(value1.getCouponAmount().add(value2.getCouponAmount()));
                        return value1;
                    }
                }, new WindowFunction<TradeSkuOrderBean, TradeSkuOrderBean, String, TimeWindow>() {
                    @Override
                    public void apply(String s, TimeWindow window, Iterable<TradeSkuOrderBean> input, Collector<TradeSkuOrderBean> out) throws Exception {
                        //取出数据
                        TradeSkuOrderBean next = input.iterator().next();
                        //补充字段
                        next.setTs(System.currentTimeMillis());
                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));
                        //输出数据
                        out.collect(next);
                    }
                });

        reduceDS.print("reduceDS>>>>>>>>>");

        //TODO 8.关联维表
//        reduceDS.map(new MapFunction<TradeSkuOrderBean, TradeSkuOrderBean>() {
//            @Override
//            public TradeSkuOrderBean map(TradeSkuOrderBean value) throws Exception {
//                DruidPooledConnection connection = DruidDSUtil.getPhoenixConn();
//                //查询SKU
//                JSONObject dimSkuInfo = DimUtil.getDimInfo(connection, "DIM_SKU_INFO", value.getSkuId());
//                value.setSkuName(dimSkuInfo.getString("SKU_NAME"));
//                value.setSpuId(dimSkuInfo.getString("SPU_ID"));
//                //查询SPU
//                JSONObject dimSpuInfo = DimUtil.getDimInfo(connection, "DIM_SPU_INFO", value.getSpuId());
//                value.setSpuName(dimSpuInfo.getString("SPU_NAME"));
//                //查询Trademark
//                //查询Category3
//                //查询Category2
//                //查询Category1
//                return null;
//            }
//        });
        //8.1 关联SKU
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithSkuDS = AsyncDataStream.unorderedWait(reduceDS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_SKU_INFO") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        //System.out.println("SKU_ID:" + input.getSkuId());
                        return input.getSkuId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setSkuName(dimInfo.getString("SKU_NAME"));
                        input.setSpuId(dimInfo.getString("SPU_ID"));
                        input.setTrademarkId(dimInfo.getString("TM_ID"));
                        input.setCategory3Id(dimInfo.getString("CATEGORY3_ID"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        tradeSkuOrderWithSkuDS.print("tradeSkuOrderWithSkuDS>>>>>>>");

        //关联SPU
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithSpuDS = AsyncDataStream.unorderedWait(tradeSkuOrderWithSkuDS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_SPU_INFO") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        return input.getSpuId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setSpuName(dimInfo.getString("SPU_NAME"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        //关联Trademark
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithTrademarkDS = AsyncDataStream.unorderedWait(tradeSkuOrderWithSpuDS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_BASE_TRADEMARK") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        return input.getTrademarkId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setTrademarkName(dimInfo.getString("TM_NAME"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        //关联Category3
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithCategory3DS = AsyncDataStream.unorderedWait(tradeSkuOrderWithTrademarkDS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_BASE_CATEGORY3") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        return input.getCategory3Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setCategory2Id(dimInfo.getString("CATEGORY2_ID"));
                        input.setCategory3Name(dimInfo.getString("NAME"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        //关联Category2
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithCategory2DS = AsyncDataStream.unorderedWait(tradeSkuOrderWithCategory3DS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_BASE_CATEGORY2") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        return input.getCategory2Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setCategory1Id(dimInfo.getString("CATEGORY1_ID"));
                        input.setCategory2Name(dimInfo.getString("NAME"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        //关联Category1
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderWithCategory1DS = AsyncDataStream.unorderedWait(tradeSkuOrderWithCategory2DS,
                new AsyncDimFunction<TradeSkuOrderBean>("DIM_BASE_CATEGORY1") {
                    @Override
                    public String getKey(TradeSkuOrderBean input) {
                        return input.getCategory1Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean input, JSONObject dimInfo) {
                        input.setCategory1Name(dimInfo.getString("NAME"));
                    }
                },
                60,
                TimeUnit.SECONDS);

        //TODO 9.将数据写出
        tradeSkuOrderWithCategory1DS.print("tradeSkuOrderWithCategory1DS>>>>>>>>");
        tradeSkuOrderWithCategory1DS.addSink(ClickHouseUtil.getSinkFunction("insert into dws_trade_sku_order_window values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

        //TODO 10.启动
        env.execute("Dws09TradeSkuOrderWindow");

    }
}
