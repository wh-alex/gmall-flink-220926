package com.atguigu.app.dws;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TradeSkuOrderBean;
import com.atguigu.utils.MyKafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.hadoop.yarn.webapp.hamlet2.Hamlet;

import java.math.BigDecimal;
import java.time.Duration;

public class Dws09TradeSkuOrderWindow_02 {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.读取Kafka DWD层 订单明细主题数据
        String topic = "dwd_trade_order_detail";
        String groupId = "dws_trade_sku_order_220926";
        DataStreamSource<String> kafkaDS = env.addSource(MyKafkaUtil.getFlinkKafkaConsumer(topic, groupId));

        //TODO 3.过滤null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    JSONObject jsonObject = JSONObject.parseObject(value);
                    out.collect(jsonObject);
                }
            }
        });

        //TODO 4.提取时间戳
        SingleOutputStreamOperator<JSONObject> jsonObjWithWMDS = jsonObjDS.assignTimestampsAndWatermarks(WatermarkStrategy.<JSONObject>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
            @Override
            public long extractTimestamp(JSONObject element, long recordTimestamp) {
                return element.getLong("create_time");
            }
        }));

        //TODO 5.按照订单明细ID分组
        KeyedStream<JSONObject, String> keyedByDetailIdDS = jsonObjWithWMDS.keyBy(json -> json.getString("id"));

        //TODO 6.去重并转换为JavaBean对象
        //假设
        //split_total_amount、split_original_amount来自于左表
        //splitActivityAmount                     来自于右1
        //splitCouponAmount                       来自于右2
        keyedByDetailIdDS.flatMap(new RichFlatMapFunction<JSONObject, TradeSkuOrderBean>() {

            private ValueState<String> leftState;
            private ValueState<String> right1State;
            private ValueState<String> right2State;

            @Override
            public void open(Configuration parameters) throws Exception {
                leftState = getRuntimeContext().getState(new ValueStateDescriptor<String>("left", String.class));
                right1State = getRuntimeContext().getState(new ValueStateDescriptor<String>("right-1", String.class));
                right2State = getRuntimeContext().getState(new ValueStateDescriptor<String>("right-2", String.class));
            }

            @Override
            public void flatMap(JSONObject value, Collector<TradeSkuOrderBean> out) throws Exception {

                //取出状态值
                String left = leftState.value();
                String right1 = right1State.value();
                String right2 = right2State.value();

                TradeSkuOrderBean.TradeSkuOrderBeanBuilder tradeSkuOrderBeanBuilder =
                        TradeSkuOrderBean
                                .builder()
                                .skuId("")
                                .skuName("");

                if (left == null) {
                    tradeSkuOrderBeanBuilder
                            .orderAmount(value.getBigDecimal(""))
                            .originalAmount(value.getBigDecimal(""));
                    leftState.update("1");
                } else {
                    tradeSkuOrderBeanBuilder
                            .orderAmount(new BigDecimal("0.0"))
                            .originalAmount(new BigDecimal("0.0"));
                }

                if (right1 == null) {
                    BigDecimal splitActivityAmount = value.getBigDecimal("split_activity_amount");
                    if (splitActivityAmount != null) {
                        tradeSkuOrderBeanBuilder.activityAmount(splitActivityAmount);
                        right1State.update("1");
                    } else {
                        tradeSkuOrderBeanBuilder.activityAmount(new BigDecimal("0.0"));
                    }
                } else {
                    tradeSkuOrderBeanBuilder.activityAmount(new BigDecimal("0.0"));
                }

                if (right2 == null) {
                    BigDecimal splitCouponAmount = value.getBigDecimal("split_coupon_amount");
                    if (splitCouponAmount != null) {
                        tradeSkuOrderBeanBuilder.couponAmount(splitCouponAmount);
                        right2State.update("1");
                    } else {
                        tradeSkuOrderBeanBuilder.couponAmount(new BigDecimal("0.0"));
                    }
                } else {
                    tradeSkuOrderBeanBuilder.couponAmount(new BigDecimal("0.0"));
                }

                out.collect(tradeSkuOrderBeanBuilder.build());
            }
        });


    }

}
