package com.atguigu.app.dws;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TradeSkuOrderBean;
import com.atguigu.utils.MyKafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.math.BigDecimal;
import java.time.Duration;

public class Dws09TradeSkuOrderWindow_03 {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.读取Kafka DWD层 订单明细主题数据
        String topic = "dwd_trade_order_detail";
        String groupId = "dws_trade_sku_order_220926";
        DataStreamSource<String> kafkaDS = env.addSource(MyKafkaUtil.getFlinkKafkaConsumer(topic, groupId));

        //TODO 3.过滤null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    JSONObject jsonObject = JSONObject.parseObject(value);
                    out.collect(jsonObject);
                }
            }
        });

        //TODO 4.提取时间戳
        SingleOutputStreamOperator<JSONObject> jsonObjWithWMDS = jsonObjDS.assignTimestampsAndWatermarks(WatermarkStrategy.<JSONObject>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
            @Override
            public long extractTimestamp(JSONObject element, long recordTimestamp) {
                return element.getLong("create_time");
            }
        }));

        //TODO 5.按照订单明细ID分组
        KeyedStream<JSONObject, String> keyedByDetailIdDS = jsonObjWithWMDS.keyBy(json -> json.getString("id"));

        //TODO 6.去重并转换为JavaBean对象
        keyedByDetailIdDS.process(new KeyedProcessFunction<String, JSONObject, TradeSkuOrderBean>() {

            private ValueState<JSONObject> valueState;

            @Override
            public void open(Configuration parameters) throws Exception {
                valueState = getRuntimeContext().getState(new ValueStateDescriptor<JSONObject>("value-state", JSONObject.class));
            }

            @Override
            public void processElement(JSONObject value, KeyedProcessFunction<String, JSONObject, TradeSkuOrderBean>.Context ctx, Collector<TradeSkuOrderBean> out) throws Exception {

                JSONObject state = valueState.value();

                if (state == null) {
                    valueState.update(value);
                    TimerService timerService = ctx.timerService();
                    long ts = timerService.currentProcessingTime();
                    timerService.registerProcessingTimeTimer(ts + 5000L);
                } else {
                    if (value.getLong("ts") > state.getLong("ts")) {
                        valueState.update(value);
                    }
                }
            }

            @Override
            public void onTimer(long timestamp, KeyedProcessFunction<String, JSONObject, TradeSkuOrderBean>.OnTimerContext ctx, Collector<TradeSkuOrderBean> out) throws Exception {

                JSONObject value = valueState.value();
                BigDecimal splitActivityAmount = value.getBigDecimal("split_activity_amount");
                if (splitActivityAmount == null) {
                    splitActivityAmount = new BigDecimal("0.0");
                }

                BigDecimal splitCouponAmount = value.getBigDecimal("split_coupon_amount");
                if (splitCouponAmount == null) {
                    splitCouponAmount = new BigDecimal("0.0");
                }

                out.collect(TradeSkuOrderBean.builder()
                        .skuId("")
//                            .skuName("")
                        .orderAmount(value.getBigDecimal("split_total_amount"))
                        .originalAmount(value.getBigDecimal("split_original_amount"))
                        .activityAmount(splitActivityAmount)
                        .couponAmount(splitCouponAmount)
                        .build());
                valueState.clear();
            }
        });

    }
}
