package com.atguigu.app.dws;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TradeProvinceOrderWindow_02;
import com.atguigu.func.AsyncDimFunction;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.MyKafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

//数据流:web/app -> 业务服务器(Mysql) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> ClickHouse(DWS)
//程 序:Mock -> Mysql -> Maxwell -> Kafka(ZK) -> Dwd03_TradeOrderDetail -> Kafka(ZK) -> Dws10TradeProvinceOrderWindow_02(Redis,Phoenix) -> ClickHouse(ZK)
public class Dws10TradeProvinceOrderWindow_02 {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

//        env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
//        env.getCheckpointConfig().setCheckpointTimeout(10000L);
//        env.setStateBackend(new HashMapStateBackend());
//        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc/220926");

        //设置HDFS用户信息
        //System.setProperty("HADOOP_USER_NAME", "atguigu");

        //TODO 2.读取Kafka DWD层 订单明细主题数据
        String topic = "dwd_trade_order_detail";
        String groupId = "dws_province_order_220926_2";
        DataStreamSource<String> kafkaDS = env.addSource(MyKafkaUtil.getFlinkKafkaConsumer(topic, groupId));

        //TODO 3.过滤null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    JSONObject jsonObject = JSONObject.parseObject(value);
                    out.collect(jsonObject);
                }
            }
        });

        //TODO 4.提取时间戳
        SingleOutputStreamOperator<JSONObject> jsonObjWithWMDS = jsonObjDS.assignTimestampsAndWatermarks(WatermarkStrategy.<JSONObject>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
            @Override
            public long extractTimestamp(JSONObject element, long recordTimestamp) {
                return element.getLong("create_time");
            }
        }));

        //TODO 5.按照订单明细id分组去重由left join产生的重复数据,并转换为JavaBean对象
        SingleOutputStreamOperator<TradeProvinceOrderWindow_02> tradeProvinceDS = jsonObjWithWMDS.keyBy(json -> json.getString("id"))
                .flatMap(new RichFlatMapFunction<JSONObject, TradeProvinceOrderWindow_02>() {

                    private ValueState<String> detailIdValueState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        StateTtlConfig ttlConfig = new StateTtlConfig.Builder(Time.seconds(5))
                                .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                                .build();
                        ValueStateDescriptor<String> stateDescriptor = new ValueStateDescriptor<>("detail-state", String.class);
                        stateDescriptor.enableTimeToLive(ttlConfig);

                        detailIdValueState = getRuntimeContext().getState(stateDescriptor);
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<TradeProvinceOrderWindow_02> out) throws Exception {

                        //取出状态数据
                        String state = detailIdValueState.value();

                        if (state == null) {
                            HashSet<String> orderIds = new HashSet<>();
                            orderIds.add(value.getString("order_id"));
                            detailIdValueState.update("1");
                            out.collect(TradeProvinceOrderWindow_02.builder()
                                    .provinceId(value.getString("province_id"))
                                    .orderId(orderIds)
                                    .orderAmount(value.getBigDecimal("split_original_amount"))
                                    .build());
                        }
                    }
                });

        //TODO 6.按照省份ID分组开窗聚合
        SingleOutputStreamOperator<TradeProvinceOrderWindow_02> reduceDS = tradeProvinceDS.keyBy(TradeProvinceOrderWindow_02::getProvinceId)
                .window(TumblingEventTimeWindows.of(org.apache.flink.streaming.api.windowing.time.Time.seconds(10)))
                .reduce(new ReduceFunction<TradeProvinceOrderWindow_02>() {
                    @Override
                    public TradeProvinceOrderWindow_02 reduce(TradeProvinceOrderWindow_02 value1, TradeProvinceOrderWindow_02 value2) throws Exception {
                        value1.setOrderAmount(value1.getOrderAmount().add(value2.getOrderAmount()));
                        value1.getOrderId().addAll(value2.getOrderId());
                        return value1;
                    }
                }, new WindowFunction<TradeProvinceOrderWindow_02, TradeProvinceOrderWindow_02, String, TimeWindow>() {
                    @Override
                    public void apply(String s, TimeWindow window, Iterable<TradeProvinceOrderWindow_02> input, Collector<TradeProvinceOrderWindow_02> out) throws Exception {
                        //取出数据
                        TradeProvinceOrderWindow_02 next = input.iterator().next();
                        //补充字段
                        next.setTs(System.currentTimeMillis());
                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));

                        next.setOrderCount((long) next.getOrderId().size());
                        //输出数据
                        out.collect(next);
                    }
                });

        reduceDS.print("reduceDS>>>>>>>>>>>>>");

        //TODO 7.关联维表补充省份名称
        SingleOutputStreamOperator<TradeProvinceOrderWindow_02> resultDS = AsyncDataStream.unorderedWait(reduceDS,
                new AsyncDimFunction<TradeProvinceOrderWindow_02>("DIM_BASE_PROVINCE") {
                    @Override
                    public String getKey(TradeProvinceOrderWindow_02 input) {
                        return input.getProvinceId();
                    }

                    @Override
                    public void join(TradeProvinceOrderWindow_02 input, JSONObject dimInfo) {
                        input.setProvinceName(dimInfo.getString("NAME"));
                    }
                }, 60, TimeUnit.SECONDS);

        //TODO 9.将数据写出
        resultDS.print(">>>>>>>>>");
        //resultDS.addSink(ClickHouseUtil.getSinkFunction("insert into dws_trade_province_order_window values(?,?,?,?,?,?,?)"));

        //TODO 10.启动
        env.execute("Dws10TradeProvinceOrderWindow_02");

    }
}
