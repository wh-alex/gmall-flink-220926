package com.atguigu.func;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.DimUtil;
import com.atguigu.utils.DruidDSUtil;
import com.atguigu.utils.ThreadPoolUtil;
import lombok.SneakyThrows;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import java.util.Collections;
import java.util.concurrent.ThreadPoolExecutor;

public abstract class AsyncDimFunction<T> extends RichAsyncFunction<T, T> implements AsyncJoinFunction<T> {

    private ThreadPoolExecutor threadPoolExecutor;
    private String tableName;

    public AsyncDimFunction(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        threadPoolExecutor = ThreadPoolUtil.getThreadPoolExecutor();
    }

    @Override
    public void asyncInvoke(T input, ResultFuture<T> resultFuture) {

        threadPoolExecutor.execute(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {

                //查询维度数据
                DruidPooledConnection connection = DruidDSUtil.getPhoenixConn();
                String key = getKey(input);
                JSONObject dimInfo = DimUtil.getDimInfo(connection, tableName, key);

                //补充字段
                if (dimInfo != null) {
                    join(input, dimInfo);
                }

                connection.close();

                //返回结果
                resultFuture.complete(Collections.singleton(input));
            }
        });
    }


    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) {
        System.out.println("TimeOut:" + input);
    }
}
