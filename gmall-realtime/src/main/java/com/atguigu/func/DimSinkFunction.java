package com.atguigu.func;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.GmallConfig;
import com.atguigu.utils.DruidDSUtil;
import com.atguigu.utils.JedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import redis.clients.jedis.Jedis;

import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class DimSinkFunction extends RichSinkFunction<JSONObject> {

    //Value:{"database":"gmall-220623-flink","table":"base_trademark","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null},"sink_table":"dim_xxx_xxx"}
    @Override
    public void invoke(JSONObject value, Context context) throws Exception {

        //获取连接
        DruidPooledConnection connection = DruidDSUtil.getPhoenixConn();

        //拼接SQL  upsert into db.tn(id,name,sex) values('1001','zs','male')
        String sinkTable = value.getString("sink_table");
        JSONObject data = value.getJSONObject("data");
        String sql = genUpsertSql(sinkTable,
                data);
        System.out.println("UpsertSql：" + sql);

        //如果为更新操作,则先写入Redis
        if ("update".equals(value.getString("type"))) {
            Jedis jedis = JedisUtil.getJedis();
            String redisKey = "DIM:" + sinkTable.toUpperCase() + ":" + data.getString("id");

            JSONObject jsonObject = new JSONObject();
            Set<Map.Entry<String, Object>> entries = data.entrySet();
            for (Map.Entry<String, Object> next : entries) {
                jsonObject.put(next.getKey().toUpperCase(), next.getValue().toString());
            }

            jedis.setex(redisKey, 24 * 3600, jsonObject.toJSONString());

            jedis.close();
        }

        //插入数据
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();
        connection.commit();

        //归还连接
        connection.close();
    }

    //获取插入数据的SQL语句:upsert into db.tn(id,name,sex) values('1001','zs','male)
    private String genUpsertSql(String sinkTable, JSONObject data) {

        //获取列名
        Set<String> columns = data.keySet();
        //获取列值
        Collection<Object> values = data.values();

        return "upsert into " + GmallConfig.HBASE_SCHEMA + "." + sinkTable + "(" +
                StringUtils.join(columns, ",") + ") values('" +
                StringUtils.join(values, "','") + "')";
    }
}
