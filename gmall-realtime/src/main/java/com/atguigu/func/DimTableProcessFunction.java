package com.atguigu.func;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.GmallConfig;
import com.atguigu.utils.DruidDSUtil;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class DimTableProcessFunction extends BroadcastProcessFunction<JSONObject, String, JSONObject> {

    private MapStateDescriptor<String, TableProcess> stateDescriptor;

    private HashMap<String, TableProcess> tableProcessHashMap;

    public DimTableProcessFunction(MapStateDescriptor<String, TableProcess> stateDescriptor) {
        this.stateDescriptor = stateDescriptor;
    }

    @Override
    public void open(Configuration parameters) throws Exception {

        //HashMap初始化
        tableProcessHashMap = new HashMap<>();

        //预加载配置信息并写入HashMap
        Connection conn = DriverManager.getConnection("jdbc:mysql://hadoop102:3306/gmall-220926-config?" + "user=root&password=000000&useUnicode=true&" +
                "characterEncoding=utf8&serverTimeZone=Asia/Shanghai&useSSL=false");
        List<TableProcess> tableProcesses = JdbcUtil.queryList(conn,
                "select * from table_process where sink_type='dim'",
                TableProcess.class,
                true);

        //遍历集合数据
        for (TableProcess tableProcess : tableProcesses) {
            //建表
            checkTable(tableProcess);
            //保存数据进HashMap
            tableProcessHashMap.put(tableProcess.getSourceTable(), tableProcess);
        }
    }

    //Value:{"before":null,"after":{"source_table":"base_category3","sink_table":"dim_base_category3","sink_columns":"id,name,category2_id","sink_pk":"id","sink_extend":null},"source":{"version":"1.5.4.Final","connector":"mysql","name":"mysql_binlog_source","ts_ms":1669162876406,"snapshot":"false","db":"gmall-220623-config","sequence":null,"table":"table_process","server_id":0,"gtid":null,"file":"","pos":0,"row":0,"thread":null,"query":null},"op":"r","ts_ms":1669162876406,"transaction":null}
    @Override
    public void processBroadcastElement(String value, BroadcastProcessFunction<JSONObject, String, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {

        //获取状态
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);

        //0.如果为删除配置信息操作,则删除状态中对应的数据
        JSONObject jsonObject = JSON.parseObject(value);

        if ("d".equals(jsonObject.getString("op"))) {
            TableProcess tableProcess = JSON.parseObject(jsonObject.getString("before"), TableProcess.class);

            if ("dim".equals(tableProcess.getSinkType())) {
                String key = tableProcess.getSourceTable();
                broadcastState.remove(key);

                //同时删除HashMap中的数据
                tableProcessHashMap.remove(key);
            }
        } else {

            //1.转换为JavaBean
            TableProcess tableProcess = JSON.parseObject(jsonObject.getString("after"), TableProcess.class);

            if ("dim".equals(tableProcess.getSinkType())) {
                //2.建表
                checkTable(tableProcess);
                //3.将数据写入状态
                broadcastState.put(tableProcess.getSourceTable(), tableProcess);
            }
        }
    }

    //建表:create table if not exists db.tn(id varchar primary key ,name varchar ,sex varchar) xxx
    private void checkTable(TableProcess tableProcess) {

        //处理特殊字段
        String sinkPk = tableProcess.getSinkPk();
        if (sinkPk == null || sinkPk.equals("")) {
            sinkPk = "id";
        }
        String sinkExtend = tableProcess.getSinkExtend();
        if (sinkExtend == null) {
            sinkExtend = "";
        }

        //拼接SQL语句
        StringBuilder sql = new StringBuilder("create table if not exists ")
                .append(GmallConfig.HBASE_SCHEMA)
                .append(".")
                .append(tableProcess.getSinkTable())
                .append("(");

        //获取字段
        String sinkColumns = tableProcess.getSinkColumns();
        String[] fields = sinkColumns.split(",");
        for (int i = 0; i < fields.length; i++) {

            String field = fields[i];
            sql.append(field).append(" varchar ");

            //判断是否为主键
            if (sinkPk.equals(field)) {
                sql.append(" primary key ");
            }

            //判断是否为最后一个字段
            if (i < fields.length - 1) {
                sql.append(",");
            } else {
                sql.append(")").append(sinkExtend);
            }
        }

        //打印SQL
        System.out.println("建表语句为：" + sql);

        try {
            //获取Phoenix连接
            DruidPooledConnection connection = DruidDSUtil.getPhoenixConn();
            //预编译SQL
            PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
            //执行建表操作
            preparedStatement.execute();
            //归还连接
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException("建表：" + tableProcess.getSinkTable() + " 失败！");
        }
    }

    //Value:{"database":"gmall-220623-flink","table":"base_trademark","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null}}
    //"type":"insert",“update”,"delete","bootstrap-start","bootstrap-insert","bootstrap-complete"
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, String, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //1.获取状态数据
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);
        String key = value.getString("table");
        TableProcess tableProcess = broadcastState.get(key);
        TableProcess mapTableProcess = tableProcessHashMap.get(key);

        //取出操作类型
        String type = value.getString("type");

        //2.1 行过滤
        if ((tableProcess != null || mapTableProcess != null) && ("insert".equals(type) || "update".equals(type) || "bootstrap-insert".equals(type))) {

            if (tableProcess == null) {
                tableProcess = mapTableProcess;
            }

            //2.2 列过滤
            filterColumn(value.getJSONObject("data"), tableProcess.getSinkColumns());

            //3.补充SinkTable并写出
            value.put("sink_table", tableProcess.getSinkTable());
            out.collect(value);
        } else {
            System.out.println("该Key：" + key + "不存在！或者操作类型为：" + type);
        }
    }

    /**
     * 数据的列过滤
     *
     * @param data        {"id":"12","tm_name":"atguigu","name":"zhangsan","logo_url":"/xxx/xxx"}
     * @param sinkColumns "id,tm_name"
     */
    private void filterColumn(JSONObject data, String sinkColumns) {

        //将SinkColumns处理成集合
        String[] columns = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(columns);

        Set<Map.Entry<String, Object>> entries = data.entrySet();
        entries.removeIf(next -> !columnList.contains(next.getKey()));

//        Set<Map.Entry<String, Object>> entries = data.entrySet();
//        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Object> next = iterator.next();
//            if (!columnList.contains(next.getKey())) {
//                iterator.remove();
//            }
//        }
    }
}
