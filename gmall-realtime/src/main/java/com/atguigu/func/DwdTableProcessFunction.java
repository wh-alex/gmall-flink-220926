package com.atguigu.func;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class DwdTableProcessFunction extends BroadcastProcessFunction<JSONObject, String, JSONObject> {

    private MapStateDescriptor<String, TableProcess> mapStateDescriptor;

    private HashMap<String, TableProcess> tableProcessHashMap;

    public DwdTableProcessFunction(MapStateDescriptor<String, TableProcess> mapStateDescriptor) {
        this.mapStateDescriptor = mapStateDescriptor;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        tableProcessHashMap = new HashMap<>();

        Connection conn = DriverManager.getConnection("jdbc:mysql://hadoop102:3306/gmall-220926-config?" + "user=root&password=000000&useUnicode=true&" +
                "characterEncoding=utf8&serverTimeZone=Asia/Shanghai&useSSL=false");
        List<TableProcess> tableProcesses = JdbcUtil.queryList(conn,
                "select * from table_process where sink_type='dwd'",
                TableProcess.class,
                true);
        for (TableProcess tableProcess : tableProcesses) {
            tableProcessHashMap.put(tableProcess.getSourceTable() + "-" + tableProcess.getSourceType(),
                    tableProcess);
        }
    }

    //Value:{"before":null,"after":{"source_table":"base_category3","sink_table":"dim_base_category3","sink_columns":"id,name,category2_id","sink_pk":"id","sink_extend":null},"source":{"version":"1.5.4.Final","connector":"mysql","name":"mysql_binlog_source","ts_ms":1669162876406,"snapshot":"false","db":"gmall-220623-config","sequence":null,"table":"table_process","server_id":0,"gtid":null,"file":"","pos":0,"row":0,"thread":null,"query":null},"op":"r","ts_ms":1669162876406,"transaction":null}
    @Override
    public void processBroadcastElement(String value, BroadcastProcessFunction<JSONObject, String, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {

        //取出状态
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);

        //如果为删除配置信息操作
        JSONObject jsonObject = JSON.parseObject(value);
        String op = jsonObject.getString("op");
        if ("d".equals(op)) {
            TableProcess before = JSON.parseObject(jsonObject.getString("before"), TableProcess.class);
            if ("dwd".equals(before.getSinkType())) {
                String key = before.getSourceTable() + "-" + before.getSourceType();
                broadcastState.remove(key);
                tableProcessHashMap.remove(key);
            }
        } else {

            //转换为JavaBean
            TableProcess tableProcess = JSON.parseObject(jsonObject.getString("after"), TableProcess.class);

            if ("dwd".equals(tableProcess.getSinkType())) {
                //写入状态
                broadcastState.put(tableProcess.getSourceTable() + "-" + tableProcess.getSourceType(), tableProcess);
            }
        }
    }

    //Value:{"database":"gmall-220623-flink","table":"base_trademark","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null}}
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, String, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //获取状态
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
        String key = value.getString("table") + "-" + value.getString("type");
        TableProcess tableProcess = broadcastState.get(key);
        TableProcess mapTableProcess = tableProcessHashMap.get(key);

        //行过滤
        if (tableProcess != null || mapTableProcess != null) {

            if (tableProcess == null) {
                tableProcess = mapTableProcess;
            }

            //列过滤
            filterColumn(value.getJSONObject("data"),
                    tableProcess.getSinkColumns());

            //补充SinkTable字段并将数据写出
            value.put("sink_table", tableProcess.getSinkTable());
            out.collect(value);

        } else {
            System.out.println(key + "--配置信息不存在！");
        }
    }

    /**
     * 数据的列过滤
     *
     * @param data        {"id":"12","tm_name":"atguigu","name":"zhangsan","logo_url":"/xxx/xxx"}
     * @param sinkColumns "id,tm_name"
     */
    private void filterColumn(JSONObject data, String sinkColumns) {

        //将SinkColumns处理成集合
        String[] columns = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(columns);

        Set<Map.Entry<String, Object>> entries = data.entrySet();
        entries.removeIf(next -> !columnList.contains(next.getKey()));

//        Set<Map.Entry<String, Object>> entries = data.entrySet();
//        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Object> next = iterator.next();
//            if (!columnList.contains(next.getKey())) {
//                iterator.remove();
//            }
//        }
    }
}
