package com.atguigu.utils;

import com.atguigu.bean.TransientSink;
import com.atguigu.common.GmallConfig;
import lombok.SneakyThrows;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ClickHouseUtil {

    //sql:insert into db.tn values(?,?,?,?,?)
    public static <T> SinkFunction<T> getSinkFunction(String sql) {
        return JdbcSink.<T>sink(sql,
                new JdbcStatementBuilder<T>() {
                    @SneakyThrows
                    @Override
                    public void accept(PreparedStatement preparedStatement, T t) throws SQLException {

                        //通过反射的方式获取所有字段信息
                        Class<?> clz = t.getClass();

//                        Method[] methods = clz.getMethods();
//                        for (int i = 0; i < methods.length; i++) {
//                            Method method = methods[i];
//                            Object invoke = method.invoke(t, "xx", "yy");
//                        }

                        Field[] fields = clz.getDeclaredFields();

                        int offset = 0;
                        for (int i = 0; i < fields.length; i++) {
                            Field field = fields[i];

                            field.setAccessible(true);

                            //尝试获取字段的标记,如果标记存在,则跳过该字段
                            TransientSink sink = field.getAnnotation(TransientSink.class);
                            if (sink != null) {
                                offset++;
                                continue;
                            }

                            Object value = field.get(t);

                            preparedStatement.setObject(i + 1 - offset, value);
                        }
                    }
                },
                new JdbcExecutionOptions.Builder()
                        .withBatchSize(5)
                        .withBatchIntervalMs(1000L)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        .withDriverName(GmallConfig.CLICKHOUSE_DRIVER)
                        .withUrl(GmallConfig.CLICKHOUSE_URL)
                        .build());
    }

}
