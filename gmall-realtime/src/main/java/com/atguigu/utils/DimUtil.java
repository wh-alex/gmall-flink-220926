package com.atguigu.utils;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.GmallConfig;
import redis.clients.jedis.Jedis;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class DimUtil {

    public static JSONObject getDimInfo(Connection connection, String tableName, String id) throws Exception {

        //查询Redis数据
        Jedis jedis = JedisUtil.getJedis();
        String redisKey = "DIM:" + tableName + ":" + id;
        String dimInfoStr = jedis.get(redisKey);
        if (dimInfoStr != null) {
            jedis.expire(redisKey, 24 * 3600);
            jedis.close();
            return JSONObject.parseObject(dimInfoStr);
        }

        //拼接SQL语句
        String sql = "select * from " + GmallConfig.HBASE_SCHEMA + "." + tableName + " where id='" + id + "'";
        //System.out.println("SQL:" + sql);

        //查询
        List<JSONObject> queryList = JdbcUtil.queryList(connection, sql, JSONObject.class, false);

        //如果queryList.size为0,则查询MySQL

        //将数据写入Redis
        JSONObject dimInfo = queryList.get(0);
        jedis.setex(redisKey, 24 * 3600, dimInfo.toJSONString());
        jedis.close();

        //返回结果
        return dimInfo;
    }

    public static void main(String[] args) throws Exception {

        DruidPooledConnection phoenixConn = DruidDSUtil.getPhoenixConn();

        long start = System.currentTimeMillis();
        JSONObject jsonObject = getDimInfo(phoenixConn, "DIM_BASE_TRADEMARK", "16");//110 112 106
        long end = System.currentTimeMillis();
        JSONObject jsonObject2 = getDimInfo(phoenixConn, "DIM_BASE_TRADEMARK", "15");//10 8   8
        //2  2  1  1
        long end2 = System.currentTimeMillis();

        System.out.println(jsonObject);

        System.out.println(end - start);
        System.out.println(end2 - end);

        phoenixConn.close();
    }
}
