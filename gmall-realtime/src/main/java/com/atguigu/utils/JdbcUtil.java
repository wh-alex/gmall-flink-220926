package com.atguigu.utils;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.google.common.base.CaseFormat;
import org.apache.commons.beanutils.BeanUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * select count(*) from t;                       单行单列
 * select * from t where id='xx';id为主键         单行多列
 * select count(*) from t group by tm_name;      多行单列
 * select * from t;                              多行多列
 */
public class JdbcUtil {

    public static <T> List<T> queryList(Connection connection, String sql, Class<T> clz, boolean isUnderScoreToCamel) throws Exception {

        //创建集合用于存放结果数据
        ArrayList<T> list = new ArrayList<>();

        //预编译SQL
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        //执行查询
        ResultSet resultSet = preparedStatement.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        //遍历结果集,将每行数据封装为T对象
        while (resultSet.next()) {//行遍历

            //创建T对象
            T t = clz.newInstance();

            for (int i = 0; i < columnCount; i++) {//列遍历
                //获取列名和列值
                String columnName = metaData.getColumnName(i + 1);
                Object value = resultSet.getObject(columnName);

                if (isUnderScoreToCamel) {
                    columnName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, columnName.toLowerCase());
                }
                //将属性以及值设置进T对象
                BeanUtils.setProperty(t, columnName, value);
            }
            //将T对象放入集合
            list.add(t);
        }

        //返回结果
        return list;
    }

    public static void main(String[] args) throws Exception {

//        DruidPooledConnection phoenixConn = DruidDSUtil.getPhoenixConn();
        Connection conn = DriverManager.getConnection("jdbc:mysql://hadoop102:3306/gmall-220926-config?" +
                "user=root&password=000000&useUnicode=true&" +
                "characterEncoding=utf8&serverTimeZone=Asia/Shanghai&useSSL=false");

        List<TableProcess> jsonObjects = queryList(conn,
                "select * from table_process",
                TableProcess.class,
                true);
        for (TableProcess jsonObject : jsonObjects) {
            System.out.println(jsonObject);
        }
        conn.close();

    }

}
