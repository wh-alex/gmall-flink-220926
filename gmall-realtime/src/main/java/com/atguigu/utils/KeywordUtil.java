package com.atguigu.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class KeywordUtil {

    public static List<String> splitKeyword(String keyword) throws IOException {

        //创建集合用于存放结果数据
        ArrayList<String> list = new ArrayList<>();

        //创建IK分词对象
        IKSegmenter ikSegmenter = new IKSegmenter(new StringReader(keyword), false);

        //获取切分后的词
        Lexeme next = ikSegmenter.next();

        while (next != null) {
            String word = next.getLexemeText();
            list.add(word);

            next = ikSegmenter.next();
        }

        //返回结果
        return list;
    }

    public static void main(String[] args) throws IOException {

        System.out.println(splitKeyword("尚硅谷Flink实时数仓项目"));

    }

}
