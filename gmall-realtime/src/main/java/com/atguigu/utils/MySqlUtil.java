package com.atguigu.utils;

public class MySqlUtil {

    public static String getLookUpTableDDL(String tableName) {
        return "with (\n" +
                "    'connector' = 'jdbc',\n" +
                "    'url' = 'jdbc:mysql://hadoop102:3306/gmall-220926-flink',\n" +
                "    'table-name' = '" + tableName + "',\n" +
                "    'username' = 'root',\n" +
                "    'password' = '000000',\n" +
                "    'lookup.cache.max-rows' = '10',\n" +
                "    'lookup.cache.ttl' = '1 hour',\n" +
                "    'driver' = 'com.mysql.cj.jdbc.Driver'\n" +
                ")";
    }

    public static String getBaseDicLookUpTable() {
        return "create table base_dic(\n" +
                "    `dic_code` STRING,\n" +
                "    `dic_name` STRING,\n" +
                "    `parent_code` STRING,\n" +
                "    `create_time` STRING,\n" +
                "    `operate_time` STRING\n" +
                ") " + getLookUpTableDDL("base_dic");
    }

}
