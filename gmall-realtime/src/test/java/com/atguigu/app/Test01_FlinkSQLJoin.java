package com.atguigu.app;

import com.atguigu.bean.Bean1;
import com.atguigu.bean.Bean2;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.util.Collector;

import java.time.Duration;

public class Test01_FlinkSQLJoin {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //设置状态保存时间
        System.out.println(tableEnv.getConfig().getIdleStateRetention());
        tableEnv.getConfig().setIdleStateRetention(Duration.ofSeconds(10));

        //准备两个流
        SingleOutputStreamOperator<Bean1> bean1DS = env.socketTextStream("hadoop102", 8888)
                .map(line -> {
                    String[] fields = line.split(",");
                    return new Bean1(fields[0], fields[1]);
                });
        SingleOutputStreamOperator<Bean2> bean2DS = env.socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] fields = line.split(",");
                    return new Bean2(fields[0], fields[1]);
                });

        bean2DS.keyBy(Bean2::getId)
                .window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<Bean2>() {
                    @Override
                    public Bean2 reduce(Bean2 value1, Bean2 value2) throws Exception {
                        value1.setSex(value1.getSex() + value2.getSex());
                        return value1;
                    }
                });

        bean2DS.keyBy(Bean2::getId)
                .process(new KeyedProcessFunction<String, Bean2, Object>() {
                    @Override
                    public void processElement(Bean2 value, KeyedProcessFunction<String, Bean2, Object>.Context ctx, Collector<Object> out) throws Exception {
                        ctx.timerService();
                    }
                });

        //将流转换为动态表
//        Table t1 = tableEnv.fromDataStream(bean1DS);
//        Table t2 = tableEnv.fromDataStream(bean2DS);
        tableEnv.createTemporaryView("t1", bean1DS);
        tableEnv.createTemporaryView("t2", bean2DS);

        //JOIN        左表:OnCreateAndWrite    右表:OnCreateAndWrite
//        tableEnv.sqlQuery("select t1.id,t1.name,t2.sex from t1 join t2 on t1.id=t2.id")
//                .execute()
//                .print();

//        //left JOIN  左表:OnReadAndWrite      右表:OnCreateAndWrite
//        tableEnv.sqlQuery("select t1.id,t1.name,t2.sex from t1 left join t2 on t1.id=t2.id")
//                .execute()
//                .print();

        Table resultTable = tableEnv.sqlQuery("select t1.id,t1.name,t2.sex from t1 left join t2 on t1.id=t2.id");
        tableEnv.createTemporaryView("result_table", resultTable);
        tableEnv.executeSql("" +
                "create table left_join_table(\n" +
                "    `id` STRING,\n" +
                "    `name` STRING,\n" +
                "    `sex` STRING,\n" +
                "    PRIMARY KEY (id) NOT ENFORCED\n" +
                ") with (\n" +
                "    'connector' = 'upsert-kafka',\n" +
                "    'topic' = 'test',\n" +
                "    'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "    'key.format' = 'json',\n" +
                "    'value.format' = 'json'\n" +
                ")");
        tableEnv.executeSql("insert into left_join_table select * from result_table");

        //right JOIN   左表:OnCreateAndWrite    右表:OnReadAndWrite
//        tableEnv.sqlQuery("select t1.id,t1.name,t2.sex from t1 right join t2 on t1.id=t2.id")
//                .execute()
//                .print();

        //full JOIN    左表:OnReadAndWrite      右表:OnReadAndWrite
//        tableEnv.sqlQuery("select t1.id,t1.name,t2.sex from t1 full join t2 on t1.id=t2.id")
//                .execute()
//                .print();

    }
}
