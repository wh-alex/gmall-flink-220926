package com.atguigu.app;

import com.atguigu.bean.Bean1;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import static org.apache.flink.table.api.Expressions.$;

public class Test02_LookUpJoin {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //创建主表
        SingleOutputStreamOperator<Bean1> bean1DS = env.socketTextStream("hadoop102", 8888)
                .map(line -> {
                    String[] fields = line.split(",");
                    return new Bean1(fields[0], fields[1]);
                });
        Table table = tableEnv.fromDataStream(bean1DS,
                $("id"),
                $("name"),
                $("pt").proctime());
        tableEnv.createTemporaryView("t1", table);

        //构建LookUp表
        tableEnv.executeSql("" +
                "create table base_dic(\n" +
                "    `dic_code` STRING,\n" +
                "    `dic_name` STRING,\n" +
                "    `parent_code` STRING,\n" +
                "    `create_time` STRING,\n" +
                "    `operate_time` STRING\n" +
                ") with (\n" +
                "    'connector' = 'jdbc',\n" +
                "    'url' = 'jdbc:mysql://hadoop102:3306/gmall-220926-flink',\n" +
                "    'table-name' = 'base_dic',\n" +
                "    'username' = 'root',\n" +
                "    'password' = '000000',\n" +
                "    'lookup.cache.max-rows' = '10',\n" +
                "    'lookup.cache.ttl' = '1 hour',\n" +
                "    'driver' = 'com.mysql.cj.jdbc.Driver'\n" +
                ")");

        //打印维表
//        tableEnv.sqlQuery("select * from base_dic")
//                .execute()
//                .print();

        //使用主流关联维表打印
        tableEnv.sqlQuery("" +
                        "select " +
                        "    t1.id," +
                        "    t1.name," +
                        "    t2.dic_name " +
                        "from t1 " +
                        "join base_dic FOR SYSTEM_TIME AS OF t1.pt AS t2 " +
                        "on t1.id=t2.dic_code")
                .execute()
                .print();

    }
}
