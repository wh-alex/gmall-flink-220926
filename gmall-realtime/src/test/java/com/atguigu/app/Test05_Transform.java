package com.atguigu.app;

import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Test05_Transform {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        SingleOutputStreamOperator<String> mapDS = env.socketTextStream("hadoop102", 9999)
                .map(line -> line);
        mapDS.print("mapDS>>>>>>>>>");

//        mapDS.shuffle();
//        mapDS.rebalance();
//        mapDS.rescale();
//        mapDS.broadcast().print("broadcast>>>>>>>").setParallelism(4);
        mapDS.global().print("global>>>>>>>>").setParallelism(4);
//        mapDS.forward().print("forward>>>>>>>>").setParallelism(2);

        env.execute();
    }

}
