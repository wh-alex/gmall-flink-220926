package com.atguigu.app;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

public class Test08_HBase {

    public static void main(String[] args) throws Exception {

        Configuration configuration = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(configuration);

        //DDL
//        Admin admin = connection.getAdmin();

        //DML
        Table table = connection.getTable(TableName.valueOf("test_0926"));
        Delete delete = new Delete("1005".getBytes());

        //添加列族
//        delete.addFamily("f2".getBytes());

        //添加列
        //delete.addColumn();
        delete.addColumns("f1".getBytes(), "name".getBytes());

        table.delete(delete);

        table.close();
        connection.close();

    }

}
